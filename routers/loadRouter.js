const express = require('express');
const router = express.Router();

const {
  createLoad,
  getLoads,
  updateLoad,
  deleteLoad,
  getLoad,
} = require('../controllers/loadController');

const authMiddleware = require('../middlewares/authMiddleware');
const { isShipper } = require('../middlewares/userMiddleware');
const { validateBody, isStatusNew, postLoad } = require('../middlewares/loadMiddleware');
const {
  findTruck,
  changeTruckStatus,
  changeLoadStatus,
} = require('../middlewares/systemMiddleware');

router.use('/loads', authMiddleware, isShipper);

router.post('/loads', validateBody, createLoad);
router.get('/loads', getLoads);
router.get('/loads/:id', getLoad);
router.put('/loads/:id', isStatusNew, validateBody, updateLoad);
router.delete('/loads/:id', isStatusNew, deleteLoad);

router.patch('/loads/:id', isStatusNew, postLoad, findTruck, changeTruckStatus, changeLoadStatus);

module.exports = router;
