const express = require('express');
const router = express.Router();

const {
  createTruck,
  getTrucks,
  assignTruck,
  updateTruck,
  deleteTruck,
} = require('../controllers/truckController');

const authMiddleware = require('../middlewares/authMiddleware');
const { isDriver } = require('../middlewares/userMiddleware');
const { validateBody, isTruckAssigned } = require('../middlewares/truckMiddleware');

router.use('/trucks', authMiddleware, isDriver);

router.get('/trucks', getTrucks);
router.post('/trucks', validateBody, createTruck);
router.patch('/trucks/:id', assignTruck);
router.put('/trucks/:id', isTruckAssigned, validateBody, updateTruck);
router.delete('/trucks/:id', isTruckAssigned, deleteTruck);

module.exports = router;
