const express = require('express');
const router = express.Router();

const { getProfile, changePassword, deleteAccount } = require('../controllers/userController');

const authMiddleware = require('../middlewares/authMiddleware');
const { isShipper } = require('../middlewares/userMiddleware');

router.use('/users/me', authMiddleware);

router.get('/users/me', getProfile);
router.patch('/users/me', changePassword);
router.delete('/users/me', isShipper, deleteAccount);

module.exports = router;
