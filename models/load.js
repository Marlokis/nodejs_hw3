const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model(
  'load',
  new Schema(
    {
      name: {
        type: String,
        required: true,
      },
      created_by: {
        type: mongoose.Types.ObjectId,
        required: true,
      },
      address: {
        type: Object,
        required: true,
      },
      dimensions: {
        type: Object,
        required: true,
      },
      payload: {
        type: Number,
        required: true,
      },
      created_date: {
        type: String,
        default: new Date(),
      },
      logs: {
        type: Array,
      },
      assigned_to: {
        type: mongoose.Types.ObjectId,
      },
      status: {
        type: String,
        default: 'NEW',
      },
      state: {
        type: String,
      },
    },
    { versionKey: false }
  )
);
