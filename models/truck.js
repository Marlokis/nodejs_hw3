const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model(
  'truck',
  new Schema(
    {
      created_by: {
        type: mongoose.Types.ObjectId,
        required: true,
      },
      assigned_to: {
        type: mongoose.Types.ObjectId,
      },
      status: {
        type: String,
        default: 'IS',
      },
      type: {
        type: String,
        required: true,
      },
      dimensions: {
        type: Object,
        required: true,
      },
      payload: {
        type: Number,
        required: true,
      },
    },
    { versionKey: false }
  )
);
