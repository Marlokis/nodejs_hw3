const config = require('config');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const Credential = require('../models/credential');
const _Error = require('../models/error');

const secret = config.get('secret');

const register = async (request, response) => {
  const { username, password, role } = request.body;

  if (!username) {
    return response
      .status(400)
      .json(new _Error({ message: 'No username field or value provided' }));
  }
  if (!password) {
    return response
      .status(400)
      .json(new _Error({ message: 'No password field or value provided' }));
  }
  if (role !== 'Driver' && role !== 'Shipper') {
    return response.status(400).json(
      new _Error({
        message: 'No role provided. Available roles are Driver and Shipper',
      })
    );
  }

  try {
    if (await User.findOne({ username })) {
      return response
        .status(400)
        .json(new _Error({ message: 'User with this name already exists' }));
    }

    const user = new User({ username, role });
    await user.save();

    const credentials = new Credential({ _id: user._id, username, password });
    await credentials.save();

    return response.json({ message: 'Success' });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const login = async (request, response) => {
  const { username, password } = request.body;

  if (!username) {
    return response
      .status(400)
      .json(new _Error({ message: 'No username field or value provided' }));
  }
  if (!password) {
    return response
      .status(400)
      .json(new _Error({ message: 'No password field or value provided' }));
  }

  try {
    const credentials = await Credential.findOne({ username, password });
    if (!credentials) {
      return response
        .status(400)
        .json(new _Error({ message: 'No user with such username and password found' }));
    }

    const user = await User.findById(credentials._id);
    if (!user) {
      return response
        .status(400)
        .json(new _Error({ message: 'No user with such username and password found' }));
    }

    return response.json({ JWT: jwt.sign(JSON.stringify(user), secret) });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

module.exports = { register, login };
