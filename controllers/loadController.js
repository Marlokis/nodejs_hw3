const Load = require('../models/load');
const _Error = require('../models/error');

const createLoad = async (request, response) => {
  const userId = request.user._id;
  const { name, address, dimensions, payload } = request.body;

  try {
    const load = new Load({ created_by: userId, name, address, dimensions, payload });
    await load.save();

    return response.json({ message: 'Success' });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const getLoads = async (request, response) => {
  const userId = request.user._id;

  try {
    const loads = await Load.find({ created_by: userId });

    return response.json({ loads });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const updateLoad = async (request, response) => {
  const loadId = request.params.id;
  const { name, address, dimensions, payload } = request.body;

  try {
    await Load.updateOne({ _id: loadId }, { name, address, dimensions, payload });

    return response.json({ message: 'Success' });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const deleteLoad = async (request, response) => {
  const loadId = request.params.id;

  try {
    await Load.deleteOne({ _id: loadId });

    return response.json({ message: 'Success' });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const getLoad = async (request, response) => {
  const loadId = request.params.id;

  try {
    const load = await Load.findById(loadId);

    return response.json({ load });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

module.exports = { createLoad, getLoads, updateLoad, deleteLoad, getLoad };
