const User = require('../models/user');
const Credential = require('../models/credential');
const _Error = require('../models/error');

const getProfile = async (request, response) => {
  const id = request.user._id;

  try {
    const user = await User.findById(id);

    if (!user) {
      return response.status(400).json(new _Error({ message: 'User not found' }));
    }

    return response.json({ user });
  } catch (error) {
    return response.status(500).json(new _Error({ message: err.message }));
  }
};

const changePassword = async (request, response) => {
  const { oldPassword, newPassword } = request.body;
  const id = request.user._id;

  if (!oldPassword) {
    return response
      .status(400)
      .json(new _Error({ message: 'No oldPassword field or value provided' }));
  }
  if (!newPassword) {
    return response
      .status(400)
      .json(new _Error({ message: 'No newPassword field or value provided' }));
  }

  try {
    const user = await Credential.findById(id);

    if (!user) {
      return response.status(400).json(new _Error({ message: 'User not found' }));
    }
    if (oldPassword !== user.password) {
      return response
        .status(400)
        .json(new _Error({ message: 'Old and current passwords should coincide' }));
    }

    await Credential.updateOne({ _id: id }, { password: newPassword });

    return response.json({ message: 'Success' });
  } catch (err) {
    return response.status(500).json(new _Error({ message: err.message }));
  }
};

const deleteAccount = async (request, response) => {
  const userId = request.user._id;

  try {
    await User.deleteOne({ _id: userId });
    await Credential.deleteOne({ _id: userId });

    return response.json({ message: 'Success' });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

module.exports = { getProfile, changePassword, deleteAccount };
