const Truck = require('../models/truck');
const _Error = require('../models/error');

const createTruck = async (request, response) => {
  const userId = request.user._id;
  const { type, dimensions, payload } = request.body;

  try {
    const truck = new Truck({ created_by: userId, type, dimensions, payload });
    await truck.save();

    return response.json({ message: 'Success' });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const getTrucks = async (request, response) => {
  const userId = request.user._id;

  try {
    const trucks = await Truck.find({ created_by: userId });

    return response.json({ trucks });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const assignTruck = async (request, response) => {
  const truckId = request.params.id;
  const userId = request.user._id;

  try {
    const assignedTrucks = await Truck.findOne({ assigned_to: userId });
    if (assignedTrucks) {
      return response.status(400).json(new _Error({ message: 'Driver can assign only one truck' }));
    }

    const user = await Truck.findByIdAndUpdate({ _id: truckId }, { assigned_to: userId });

    if (!user) {
      return response.status(400).json(new _Error({ message: 'User not found' }));
    }

    return response.json({ message: 'Success' });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const updateTruck = async (request, response) => {
  const truckId = request.params.id;
  const { type, dimensions, payload } = request.body;

  try {
    await Truck.updateOne({ _id: truckId }, { type, dimensions, payload });

    return response.json({ message: 'Success' });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const deleteTruck = async (request, response) => {
  const truckId = request.params.id;

  try {
    await Truck.deleteOne({ _id: truckId });

    return response.json({ message: 'Success' });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

module.exports = { createTruck, getTrucks, assignTruck, updateTruck, deleteTruck };
