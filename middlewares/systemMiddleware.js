const Truck = require('../models/truck');
const Load = require('../models/load');
const _Error = require('../models/error');

const findTruck = async (request, response, next) => {
  const load = request.load;

  try {
    const trucks = await Truck.find({ assigned_to: { $exists: true } });

    const filtered = trucks.filter(
      (truck) => checkStatus(truck) && checkPayload(truck, load) && checkDimensions(truck, load)
    );

    if (filtered.length !== 0) {
      request.truck = filtered[0];
      next();
    } else {
      await Load.updateOne(
        { _id: load._id },
        {
          status: 'NEW',
          logs: [
            ...load.logs,
            { message: 'No available driver found. Try later', time: new Date() },
          ],
        }
      );

      return response.json({ message: 'Success' });
    }
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const checkStatus = (truck) => truck.status === 'IS';

const checkPayload = (truck, load) => truck.payload > load.payload;

const checkDimensions = (truck, load) => {
  const truckSize = truck.dimensions;
  const loadSize = load.dimensions;

  return (
    truckSize.length > loadSize.length &&
    truckSize.width > loadSize.width &&
    truckSize.height > loadSize.height
  );
};

const changeTruckStatus = async (request, response, next) => {
  const truck = request.truck;

  try {
    await Truck.updateOne({ _id: truck._id }, { status: 'OL' });

    next();
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const changeLoadStatus = async (request, response) => {
  const load = request.load;
  const truckCreator = request.truck.created_by;

  try {
    await Load.updateOne(
      { _id: load._id },
      {
        status: 'ASSIGNED',
        state: 'En route to Pick Up',
        logs: [...load.logs, { message: 'New load state: En route to Pick Up', time: new Date() }],
        assigned_to: truckCreator,
      }
    );

    return response.json({ message: 'Success' });
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

module.exports = { findTruck, changeTruckStatus, changeLoadStatus };
