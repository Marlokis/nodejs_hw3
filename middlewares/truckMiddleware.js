const Truck = require('../models/truck');
const _Error = require('../models/error');

const validateBody = (request, response, next) => {
  const { type, dimensions } = request.body;
  const payload = parseInt(request.body.payload);

  if (!type) {
    return response.status(400).json(new _Error({ message: 'No type field or value provided' }));
  }
  if (!dimensions) {
    return response.status(400).json(new _Error({ message: 'No dimensions object provided' }));
  }

  if (!payload) {
    return response.status(400).json(new _Error({ message: 'No payload field or value provided' }));
  }

  const length = parseInt(dimensions.length);
  const width = parseInt(dimensions.width);
  const height = parseInt(dimensions.height);

  if (!length) {
    return response.status(400).json(new _Error({ message: 'No length field or value provided' }));
  }
  if (!width) {
    return response.status(400).json(new _Error({ message: 'No width field or value provided' }));
  }
  if (!height) {
    return response.status(400).json(new _Error({ message: 'No height field or value provided' }));
  }

  request.body = {
    type,
    dimensions: {
      length,
      width,
      height,
    },
    payload,
  };
  next();
};

const isTruckAssigned = async (request, response, next) => {
  const truckId = request.params.id;
  const userId = request.user._id;

  try {
    const truck = await Truck.findOne({ _id: truckId, assigned_to: userId });
    if (truck) {
      return response.status(400).json(new _Error({ message: 'Forbidden for assigned truck' }));
    }

    next();
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

module.exports = { validateBody, isTruckAssigned };
