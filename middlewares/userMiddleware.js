const _Error = require('../models/error');

const isDriver = (request, response, next) => {
  const user = request.user;

  try {
    if (user.role !== 'Driver') {
      return response
        .status(403)
        .json(new _Error({ message: "You don't have permission to access this page" }));
    }
    next();
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const isShipper = (request, response, next) => {
  const user = request.user;

  try {
    if (user.role !== 'Shipper') {
      return response
        .status(403)
        .json(new _Error({ message: "You don't have permission to access this page" }));
    }
    next();
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

module.exports = { isDriver, isShipper };
