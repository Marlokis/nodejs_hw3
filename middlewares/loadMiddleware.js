const Load = require('../models/load');
const _Error = require('../models/error');

const validateBody = (request, response, next) => {
  const { name, address, dimensions } = request.body;
  const payload = parseInt(request.body.payload);

  if (!name) {
    return response.status(400).json(new _Error({ message: "No 'name' field or value provided" }));
  }
  if (!address) {
    return response.status(400).json(new _Error({ message: "No 'address' object provided" }));
  }
  if (!dimensions) {
    return response.status(400).json(new _Error({ message: "No 'dimensions' object provided" }));
  }
  if (!payload) {
    return response
      .status(400)
      .json(new _Error({ message: "No 'payload' field or value provided" }));
  }

  const { from, to } = address;

  if (!from) {
    return response.status(400).json(new _Error({ message: "No 'from' field or value provided" }));
  }
  if (!to) {
    return response.status(400).json(new _Error({ message: "No 'to' field or value provided" }));
  }

  const length = parseInt(dimensions.length);
  const width = parseInt(dimensions.width);
  const height = parseInt(dimensions.height);

  if (!length) {
    return response
      .status(400)
      .json(new _Error({ message: "No 'length' field or value provided" }));
  }
  if (!width) {
    return response.status(400).json(new _Error({ message: "No 'width' field or value provided" }));
  }
  if (!height) {
    return response
      .status(400)
      .json(new _Error({ message: "No 'height' field or value provided" }));
  }

  request.body = {
    name,
    address,
    dimensions: {
      length,
      width,
      height,
    },
    payload,
  };
  next();
};

const isStatusNew = async (request, response, next) => {
  const loadId = request.params.id;

  try {
    const load = await Load.findById({ _id: loadId });

    if (!load) {
      return response.status(400).json(new _Error({ message: 'Load not found' }));
    }
    if (load.status !== 'NEW') {
      return response.status(400).json(new _Error({ message: 'Load status is not NEW' }));
    }

    request.load = load;
    next();
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

const postLoad = async (request, response, next) => {
  const loadId = request.params.id;

  try {
    await Load.updateOne({ _id: loadId }, { status: 'POSTED' });

    next();
  } catch (error) {
    return response.status(500).json(new _Error({ message: error.message }));
  }
};

module.exports = { validateBody, isStatusNew, postLoad };
