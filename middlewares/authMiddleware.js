const config = require('config');
const jwt = require('jsonwebtoken');

const secret = config.get('secret');

const _Error = require('../models/error');

module.exports = (request, response, next) => {
  const header = request.headers['authorization'];

  if (!header) {
    return response.status(401).json(new _Error({ message: "No 'authorization' header found" }));
  }

  const [, token] = header.split(' ');

  try {
    request.user = jwt.verify(token, secret);
    next();
  } catch (error) {
    return response.status(401).json(new _Error({ message: 'Invalid JWT' }));
  }
};
