const express = require('express');
const app = express();
const mongoose = require('mongoose');
const config = require('config');

const port = config.get('port');
const dbUri = config.get('dbUri');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const driverRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

app.use(express.json());

app.use('/api', authRouter);
app.use('/api', userRouter);
app.use('/api', driverRouter);
app.use('/api', loadRouter);

(async function () {
  try {
    await mongoose
      .connect(dbUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      })
      .then(() => console.log('MongoDB Connected...'));

    await app.listen(port, () => {
      console.log(`Server started on ${port} port...`);
    });
  } catch (error) {
    console.log('The error was occurred during connection\n', error);
  }
})();
